// Задаем игровые символы
const xSymbol = 'x';
const oSymbol = 'o';
const gameStatusContinue = "Игра продолжается"

class GameField {

    state = [
        [null, null, null],
        [null, null, null],
        [null, null, null],
    ];

    players = [
        xSymbol,
        oSymbol
    ];

    mode = this.players[0];

    // Задаем объявление победителя
    winner = {
        [xSymbol]: 'Крестики победили',
        [oSymbol]: 'Нолики победили'
    };

    getActivePlayer() {
        if (this.mode === this.players[0]) {
            return "Ход крестиков"
        } else {
            return "Ход ноликов"
        }
    }

    changeActivePlayer() {
        if (this.mode === this.players[0]) {
            this.setMode(this.players[1])
        } else {
            this.setMode(this.players[0])
        }
    }

    setMode(mode) {
        this.mode = mode
    }

    isOverGame() {
        return this.getGameFieldStatus() !== gameStatusContinue;
    };

    getState() {
        let field = ""
        for (let i = 0; i < this.state.length; i++) {
            let row = ""
            for (let j = 0; j < this.state[i].length; j++) {
                if (this.state[i][j] === null) {
                    row += "_  "
                } else {
                    row += this.state[i][j] + "  "
                }
            }
            field += row + "\n"
        }
        return field
    }

    getGameFieldStatus() {
        // Проверка по горизонтали
        for (let i = 0; i < this.state.length; i++) {
            if (this.state[i][0] === this.state[i][1] && this.state[i][1] === this.state[i][2] && this.state[i][0] != null) {
                return this.winner[this.state[i][0]]
            }
        }
        // Проверка по вертикали
        for (let i = 0; i < this.state[0].length; i++) {
            if (this.state[0][i] === this.state[1][i] && this.state[1][i] === this.state[2][i] && this.state[0][i] != null) {
                return this.winner[this.state[0][i]]
            }
        }
        // Проверка главной диагонали
        if (this.state[0][0] === this.state[1][1] && this.state[1][1] === this.state[2][2] && this.state[1][1] != null) {
            return this.winner[this.state[1][1]]
        }
        // Проверка побочной диагонали
        if (this.state[0][2] === this.state[1][1] && this.state[1][1] === this.state[2][0] && this.state[1][1] != null) {
            return this.winner[this.state[1][1]]
        }
        // Проверка незаконченной игры
        for (let i = 0; i < this.state.length; i++) {
            for (let j = 0; j < this.state[i].length; j++) {
                if (this.state[i][j] === null) {
                    return gameStatusContinue
                }
            }
        }
        // Объявление ничьей
        return "Ничья"
    }

    isCellFree(cell) {
        return cell === null;
    }

    FieldCellValue(row, column) {
        if (row > 3 || row <= 0 || column > 3 || column <= 0) {
            alert("Неверная ячейка")
            return false
        }
        const cell = this.state[row - 1][column - 1]
        if (this.isCellFree(cell)) {
            this.state[row - 1][column - 1] = this.mode
        } else {
            alert("Ячейка занята")
            return false
        }
        return true
    }
}

const gameField = new GameField();

while (!gameField.isOverGame()) {
    //игра идёт
    const state = gameField.getState()
    const activePlayer = gameField.getActivePlayer()
    let row = 0, column = 0
    do {
        row = prompt(state + "\n" + activePlayer + "\n" + "Введите номер строки")
        column = prompt(state + "\n" + activePlayer + "\n" + "Введите номер колонки")
    } while (!gameField.FieldCellValue(row, column));
    gameField.changeActivePlayer()
}
//вывод сообщения о результате игры
alert(gameField.getState() + "\n" + gameField.getGameFieldStatus())

